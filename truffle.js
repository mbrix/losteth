// Allows us to use ES6 in our migrations and tests.
require('babel-register')

module.exports = {
  networks: {
    main: {
      network_id: 1, // Ethereum public network
      host: 'localhost',
      port: 8545,
      from: '0x6Cf889DA158ABec2a130c780342C3Cf673bCb428'
	},
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*', // Match any network id
      from: '0x6Cf889DA158ABec2a130c780342C3Cf673bCb428'
    }
  }
}
