pragma solidity ^0.4.11;

import 'zeppelin-solidity/contracts/token/StandardToken.sol';
import 'zeppelin-solidity/contracts/ReentrancyGuard.sol';
import 'zeppelin-solidity/contracts/SafeMath.sol';

/**
 * @title Lost Ethereum Token
 *
 * @dev Implemantation of a proof-of-mistake token
 * @dev proof-of-burn proxy for lost Ethereum
 * @dev micro-whitepaper: http://www.losteth.com/LostEth.pdf
 */

contract LostEth is StandardToken, ReentrancyGuard {

    string public constant name = "LostEth";
    string public constant symbol = "LOST";
    uint public constant decimals = 18;
    uint public constant INITIAL_SUPPLY = 0;

    address reward_address;
    
    mapping (address => bool) private redeemed;

    function LostEth() {
       reward_address = msg.sender;
       totalSupply = INITIAL_SUPPLY;
    }

   /*
    * @dev Check whether an address has been provably burned
    * @dev This lets us treat the LostEth contract as a proof-of-burn registry
    * @param addr The address to check 
    */
    function isAddressBurned(address addr) public constant returns (uint) {
       if (redeemed[addr]) {
           return 0;
       } else {
           return addr.balance;
       }
    }


    modifier notRedeemed(address addr) {
       if (isAddressBurned(addr) == 0) {
          throw;
        }
        _;
    }

     function toAsciiBytes(address x) internal constant returns (bytes1[40]) {
         bytes1[40] memory s;
         for (uint i = 0; i < 20; i++) {
             byte b = byte(uint8(uint(x) / (2**(8*(19 - i)))));
             byte hi = byte(uint8(b) / 16);
             byte lo = byte(uint8(b) - 16 * uint8(hi));
             s[2*i] = char(hi);
             s[2*i+1] = char(lo);            
         }
         return s;
     }
     
     function char(byte b) internal constant returns (byte c) {
         if (b < 10) return byte(uint8(b) + 0x30);
         else return byte(uint8(b) + 0x57);
     }

    // This is a simplified distance function, based on the addresses being ascii compliant
    // and the same length with a limited character set
    function getEditDistance(bytes1[40] a, bytes1[40] b) constant private returns (uint) {
      uint distance = 0;
      for (uint i = 0; i < 40; i++) {
          if (a[i] != b[i]) {
             distance = distance + 1;
          }
      }
      return distance;
    }

    function redeem(address near_address)
            nonReentrant 
            notRedeemed(near_address) public returns (uint) {

            uint distance = getEditDistance(toAsciiBytes(near_address), toAsciiBytes(msg.sender));

            uint amount = near_address.balance;

            if ((distance > 0 && distance < 4) && (amount > 0)) {
                amount = amount / distance;
                redeemed[near_address] = true;
                balances[msg.sender] = balances[msg.sender].add(amount);

                // Apply reward @ 1%
                uint reward = amount / 100; 
                balances[reward_address] = balances[reward_address].add(reward);
                
                // Increase the total available supply of Lost Eth

                totalSupply = totalSupply.add(amount + reward);

                return amount;
            } else {
                return 0;
            }
    }

}
