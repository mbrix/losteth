// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'

import losteth_artifacts from '../../build/contracts/LostEth.json'
var LostEth = contract(losteth_artifacts);

var accounts;
var account;

window.App = {
  start: function() {
    var self = this;

    LostEth.setProvider(web3.currentProvider);

    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function(err, accs) {
     var pending = document.getElementById("pending");
     var connected = document.getElementById("connected");
      if (err != null) {
      	  pending.style.display='none';
          self.setStatus("There was an error connecting to the network, check metamask or local node installation.");
        return;
      }

      if (accs.length == 0) {
      	pending.style.display='none';
      	self.setStatus("Connected to network, but no accounts configured.");
        return;
      }

      pending.style.display='none';
	  console.log("connected to the Ethereum network.");
	  connected.style.display='inline';

      accounts = accs;
	  console.log(accounts);
      account = accounts[0];

     // Let's watch the typing field
    var typingTimer;                
    var doneTypingInterval = 2000;  
    var input = document.getElementById("lost_address");
    
    input.addEventListener('input', function (evt) {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);      
    });
    
    input.addEventListener('propertychange', function (evt) {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);      
    });

	input.addEventListener('paste', function (evt) {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);      
    });


    
    function doneTyping () {
    	self.checkBalance();
    } 

      self.refreshBalance();
    });
  },

  setBounty: function(Bounty) {
  	  console.log(Bounty);
    var bounty = document.getElementById("possible_value");
    bounty.innerHTML = Bounty;
  },

  setStatus: function(message) {
    var status = document.getElementById("status");
    status.innerHTML = message;
  },

  editDistance: function() {
  	  var self = this;
  	  var search_address = self.validate_address(document.getElementById("lost_address").value);
  	  if (!self.validate_address(search_address)) {
  	  	  console.log("Address invalid");
  	  	  return 0;
	  }
	  var lower = search_address.toLowerCase();
  	  var distance = 0;
  	  for (var i=0; i < account.length; i++) {
  	  	  if (account[i] != lower[i]) {
  	  	  	  distance += 1;
		  }
	  }
	  if (distance > 0 && distance < 4) {
	  	  return distance;
	  } else {
	  	  return 0;
	  }
  },

  checkBalance: function() {
  	  var self = this;
  	  var search_address = self.validate_address(document.getElementById("lost_address").value);
  	  if (!self.validate_address(search_address)) {
  	  	  self.setBounty("");
  	  	  return;
	  }
	
	  var lost;
	  LostEth.deployed().then(function(instance) {
		  lost = instance;
		  return lost.isAddressBurned.call(search_address, {from: account});
	}).then(function(value) {
		console.log(web3.eth.getBalance(search_address).toString());
		if (value.isZero()) {
		  self.setBounty("This address has no LOST tokens");
		} else {
			var distance = self.editDistance();
			if (distance == 0) {
				self.setBounty("No LOST tokens can be redeemed by your currently active address");
			} else {
				self.setBounty("Your address " + account + " can redeem " + web3.fromWei(value/distance, 'ether') + " LOST tokens from " + search_address);
			}
		}
	});
  },

  refreshBalance: function() {
    var self = this;

    var lost;
    LostEth.deployed().then(function(instance) {
      lost = instance;
      return lost.balanceOf.call(account, {from: account});
    }).then(function(value) {
      var balance_element = document.getElementById("balance");
      balance_element.innerHTML = web3.fromWei(value.valueOf(), 'ether');
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error getting balance; see log.");
    });
  },

  validate_address: function(addr) {
  	  if (web3.isAddress(addr)) {
  	  	  return addr;
	  } else {
	  	  return false;
      }
  },

  find: function() {
      var self = this;
  	  var search_address = self.validate_address(document.getElementById("lost_address").value);
  	  if (!search_address) {
  	  	  console.log("Invalid search address");
  	  	  self.setStatus("That is an invalid address.");
  	  	  return;
	  }

  	  this.setStatus("Searching for LOST ether.... (please wait");
  	  var lost;
  	  LostEth.deployed().then(function(instance) {
  	  	  lost = instance;
  	  	  return lost.redeem(search_address, {from: account});
	  }).then(function() {
	  	  self.setBounty("");
	  	  self.setStatus("Search complete");
	  	  self.refreshBalance();
	  }).catch(function(e) {
	  	  console.log(e);
	  	  self.setStatus("Error sending to the Etheruem network");
	  });
  }
};

window.addEventListener('load', function() {
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have no accounts configured, ensure you've configured that source properly.")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }

  App.start();
});
